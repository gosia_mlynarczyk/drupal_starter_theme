/**
 * @file.
 * JavaScript for the site header.
 */

((Drupal) => {
  Drupal.behaviors.header = {
    /**
     * Attach header behavior.
     *
     * @param {object} context
     *   DOM object.
     */
    attach(context) {
      const header = context.querySelector(".header");

      if (header.length) {
        console.log("header script");
      }
    },
  };
})(Drupal);
