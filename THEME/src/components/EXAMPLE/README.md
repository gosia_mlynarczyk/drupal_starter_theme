# What is it
This is a skeleton of a FE component. It can be used as a template for new components. 

## How to use it
- Create a folder for your new component in `src/components` directory
- Copy the contents of EXAMPLE folder into `src/components/YOUR_NEW_COMPONENT_NAME`
- Remove files you don't need (most components won't need a `.js` file)
- Rename all files with your new component name and replace the word EXAMPLE with the name of your component
- Define a Drupal library for your component which registers its CSS/JS files with your theme (see below)

### How to create a Drupal library for a component 
Each component needs to have its own Drupal library (a set of files associated with it, usually CSS and JS files).

#### Defining a library
Defining a library is done via `THEME.libraries.yml` file. For instance, to add library for our EXAMPLE component,
the following code needs to be added:
````yaml
EXAMPLE:
  css:
    theme:
      dist/css/EXAMPLE/EXAMPLE.css: {}
  js:
    dist/js/EXAMPLE/EXAMPLE.js: {}
  dependencies:
   - core/drupal
   - [...other...]
````
Note that while this example demonstrates adding a single css and js file + drupal library as a dependency. 
There are significantly more options available when defining libraries. 

#### Attaching a library
Component's library can be attached to its Twig template by adding the following line:
`````
{{ attach_library('THEME/EXAMPLE') }}
`````
So, for instance, if the theme name is "my_theme" and the library name is "header", the following line has to be added
to `header.twig` file:
`````
{{ attach_library('my_theme/header') }}
`````
