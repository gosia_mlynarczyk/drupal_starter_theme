/**
 * @file.
 * JavaScript for the example component.
 */

((Drupal) => {
  Drupal.behaviors.example = {
    /**
     * Attach example behavior.
     *
     * @param {object} context
     *   DOM object.
     */
    attach(context) {
      const example = context.querySelector(".example");

      if (example) {
        console.log("example script");
      }
    },
  };
})(Drupal);
