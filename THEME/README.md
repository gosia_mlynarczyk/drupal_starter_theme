# Prerequisites
This theme assumes that you have:
* [Node](https://nodejs.org) >= 16.8.0 - this theme was successfully tested using version 16.14.0
* NPM (Node Package Manager) >= 8.0.0 - this theme was successfully tested using version 8.3.1
* Drupal [Component Library](https://www.drupal.org/project/components) module installed and enabled (to register and use custom Twig namespaces for Twig files in `src/` folder - see `THEME.info.yml` file)

# QuickStart
First you'll need to install all required packages for the theme:

**Docksal users:** Remember that when using Docksal, all npm commands should be run inside the container, so, for instance, you need to run `fin run npm install` rather than `npm install`.

```bash
$ cd themes/custom/THEME/
$ npm install
```
# Build tasks
This theme uses npm only.

## Compile everything:
```bash
$ npm run build
```
or if using Docksal:
```bash
$ fin run npm run build
```

This command will run the following scripts:
* `build:css` (lints and compiles SCSS to CSS)
* `build:js` (lints and compiles JS)
* `build:assets` (optimises theme images and svgs)

## Compile CSS
```bash
$ npm run build:css
```

This command will run the following scripts:
* `scsslint` (checks code in SCSS files against a set of rules defined in `.stylelintrc.json` file)
* `scss` (compiles SCSS files from `src` folder into `*.css` files placed in `dist` folder. Each component has its own `.css` file. All base styles (from `base` folder) are compiled into a single `base.css` file)
* `postcss` (adds vendor prefixes to `dist/styles.css` file for target browsers defined in `.browserlistrc` file).

## Compile JS
```bash
$ npm run build:js
```

This command will run the following scripts:
* `jslint` (checks code in JS files against a set of rules defined in `.eslintrc.json` - extends Drupal standards)
* `babel` (compiles JS code from files in `src` folder into code compatible with browsers defined in `.browserslistrc` placed in `dist/js` folder)


## Watch for changes:

### CSS:
```bash
$ npm run watch:css
```

### JS
```bash
$ npm run watch:js
```

### All:
```bash
$ npm run watch
```

This command will run css & js changes.

## Minify images:
```bash
$ npm run images
```

This command will run the following scripts:
* `images:delete` (deletes all images from `dist/images` folder)
* `images:minify` (minifies all images from `src/images` folder and places them in `dist/images`)


## SVGs:
All SVG icons should be placed in the `src/images/svgs` directory.

When `npm run build` task is run, SVGs in that folder will be cleaned and optimised (all redundant and useless information will be removed, IDs and classes will be prefixed with the SVG filename or an arbitrary string, etc.) - these options are set in the `svgo.config.js` file.

Optimised SVGs will be placed in the `dist/images` folder.

Thanks to a postcss task, it's possible to use those SVGs in stylesheets as encoded images, for example:
`background-image: svg-load('images/test-icon.svg');`
will be compiled to:
`background-image: url("data:image/svg+xml;charset[....]");`

See [https://github.com/TrySound/postcss-inline-svg](https://github.com/TrySound/postcss-inline-svg) for further instructions on how to inline SVGs.


# Folder structure
```
|- .storybook/           # contains storybook config files
|- src/
| |- abstracts/          # contains theme variables, functions & mixins
| |- base/               # contains styling for basic elements - forms, text, headings, links, etc.
| |- components/         # each component folder contains its Twig template, SCSS, and JS
| |  |- header/
| |  |- footer/
| |  |- ...
| |- images/             # contains all theme images
| |  |- svgs/            # contains all SVGs
|- dist/                 # contains compiled assets
|  |- css/               # compiled CSS
|  |- images/            # optimised theme images and svgs
|  |- js/                # compiled JS
|- templates/            # Drupal Twig templates. Component Twig templates can be injected into these files
|- .babelrc              # babel config
|- .browserslistrc       # defines target browsers
|- .eslintrc.json        # config for JS linting
|- .stylelintrc.json     # config for SCSS linting
|- .postcss.config.js    # config for postcss tasks
|- svgo.config.js        # config for optimising SVG icons ('svgo' build task)
|- THEME.breakpoints.yml # Drupal's breakpoints config file
|- THEME.info.yml        # Drupal's theme config file
|- THEME.libraries.yml   # Drupal's libraries config file
|- THEME.theme           # Drupal's .theme file
```

More information about Drupal theme structure and Drupal theme config files can be found here:
https://www.drupal.org/docs/theming-drupal.

# Browser support
This theme is set up to support 2 latest versions of browsers with global use > 0.5% (see `.browserslistrc` file).
If you want to check which browsers that includes, run:
```bash
$ npx browserslist
```
This data is used by different packages, such as autoprefixer and babel.

For more information see [browserlist npm package](https://www.npmjs.com/package/browserslist).

# How to use component Twig templates in Drupal template files
To include a component Twig template (from `src/components/COMPONENT` folder) add the following statement to a Drupal template (in `templates`/ folder):
```
{% include '@components/COMPONENT.twig' %}
```
(for example, see include statements for header and footer components in `templates/page.html.twig` file )

If you need to include a component template and pass variables to it, use `with` keyword, for example:
```
{% include '@components/COMPONENT.twig' with {
  title: content.field_title,
  subtitle: 'Component subtitle'|t,
  description: content.field_description,
%}
```
Make sure you also attach that component's library (its CSS and JS), as defined in `THEME.libraries.yml` file. For example:
```
{{ attach_library('THEME/header') }}

{% include '@components/header.twig' %}
```
