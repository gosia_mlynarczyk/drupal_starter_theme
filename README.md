## What is it
Drupal starter theme is a starting point for creating a custom Drupal 8/9 theme. It's a scaffolding for a theme and a set of build tasks, rather than an actual theme that can be used as-is.

## How to use it
1. Create a folder for your new theme in `web/themes/custom/your_theme_name`
2. Copy the contents of `THEME` folder into `web/themes/custom/your_theme_name`
3. Replace the word 'THEME' in Drupal theme config file names with your theme's name (for example, `THEME.info.yml` -> `your_theme_name.info.yml`)
4. Search for THEME in files and replace it with your theme's name (for example, in `package.json`, `README.md`, `your_theme_name.breakpoints.yml` and `your_theme_name.info.yml`)
5. Install and enable [Drupal Components module](https://www.drupal.org/project/components)
6. Enable your theme and set it as default (log in to Drupal as an admin user and go to `/admin/appearance` )

## Next steps
See `README.md` file in the theme folder for more information about the theme and how to use it.
